package main

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"log"
	"text/template"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"

	api "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-api-v2"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-frontend/helpers"
)

func main() {
	// Parse the optional port flag
	port := flag.Int("port", 80, "port on which to run the web server")
	flag.Parse()

	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"formatTime": helpers.FormatTime,
		"formatDate": helpers.FormatDate,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	app.Get("/", func(c *fiber.Ctx) error {
		// // Fetch the people from the backend
		// client, err := api.NewClientWithResponses("https://brp-api-mock.apps.digilab.network/haalcentraal/api/brp")
		// if err != nil {
		// 	return err
		// }

		// var query api.PersonenQuery
		// // query.FromRaadpleegMetBurgerservicenummer(api.RaadpleegMetBurgerservicenummer{
		// // 	Burgerservicenummer: []string{"999993653"},
		// // 	Fields:              []string{"burgerservicenummer", "geboorte.datum", "naam.voornamen", "naam.geslachtsnaam", "gemeenteVanInschrijving"},
		// // })

		// query.FromZoekMetNaamEnGemeenteVanInschrijving(api.ZoekMetNaamEnGemeenteVanInschrijving{
		// 	Voornamen:               "Suzanne",
		// 	Geslachtsnaam:           "Moulin",
		// 	GemeenteVanInschrijving: "0599",
		// 	Fields:                  []string{"burgerservicenummer", "geboorte.datum", "naam.voornamen", "naam.geslachtsnaam"},
		// })

		// ctx := context.Background()

		// resp, err := client.PersonenWithResponse(ctx, query)
		// if err != nil {
		// 	return err
		// }

		// log.Printf("----> resp: %s", resp.Body)

		// if err := backend.Request("GET", fmt.Sprintf("/people?%s", query.Encode()), nil, &response); err != nil {
		// 	return fmt.Errorf("error fetching people: %v", err)
		// }

		return c.Render("index", fiber.Map{})
	})

	people := app.Group("/people")

	// Route to view person details
	people.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the person from the backend
		client, err := api.NewClientWithResponses("https://brp-api-mock.apps.digilab.network/haalcentraal/api/brp")
		if err != nil {
			return err
		}

		var query api.PersonenQuery
		query.FromRaadpleegMetBurgerservicenummer(api.RaadpleegMetBurgerservicenummer{
			Burgerservicenummer: []string{c.Params("id")},
			Fields:              []string{"burgerservicenummer", "geboorte", "naam", "geslacht", "gemeenteVanInschrijving", "overlijden", "partners", "kinderen"},
		})

		ctx := context.Background()

		resp, err := client.PersonenWithResponse(ctx, query)
		if err != nil {
			return err
		}

		if resp.StatusCode() != 200 {
			// Show the repsonse
			return errors.New(string(resp.Body)) // IMPROVE: better formatting
		}

		personResp, err := resp.JSON200.AsRaadpleegMetBurgerservicenummerResponse()
		if err != nil {
			return err
		}

		if personResp.Personen == nil || len(*personResp.Personen) == 0 {
			return errors.New("geen personen in response")
		}

		persoon := (*personResp.Personen)[0]

		return c.Render("person-details", fiber.Map{"person": persoon})
	})

	// Start server
	if err := app.Listen(fmt.Sprintf(":%d", *port)); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
