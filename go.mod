module gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/frp-frontend

go 1.21.5

toolchain go1.22.1

replace gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-api-v2 => ../fictief-register-personen-api-v2

require (
	github.com/gofiber/fiber/v2 v2.52.4
	github.com/gofiber/template/html/v2 v2.1.1
	github.com/google/uuid v1.6.0
	// gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-api-v2 v0.0.0-20240416115056-06cf66d8e3fb
	gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-personen-api-v2 v0.0.0-00010101000000-000000000000
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/getkin/kin-openapi v0.123.0 // indirect
	github.com/go-chi/chi/v5 v5.0.12 // indirect
	github.com/go-openapi/jsonpointer v0.21.0 // indirect
	github.com/go-openapi/swag v0.23.0 // indirect
	github.com/gofiber/template v1.8.3 // indirect
	github.com/gofiber/utils v1.1.0 // indirect
	github.com/invopop/yaml v0.2.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/oapi-codegen/runtime v1.1.1 // indirect
	github.com/perimeterx/marshmallow v1.1.5 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.52.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
